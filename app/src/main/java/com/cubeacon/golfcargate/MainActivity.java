package com.cubeacon.golfcargate;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.content.*;

public class MainActivity extends AppCompatActivity implements OnClickListener {

  private Button scanBtn, submitBtn;
  private TextView formatTxt, contentTxt;
  private EditText etPassNumb;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    this.etPassNumb = (EditText)findViewById(R.id.eTPassengerNumber);
    this.submitBtn = (Button)findViewById(R.id.btnSubmit);
    this.submitBtn.setOnClickListener(this);

    this.scanBtn = (Button)findViewById(R.id.scan_button);
    this.formatTxt = (TextView)findViewById(R.id.scan_format);
    this.contentTxt = (TextView)findViewById(R.id.scan_content);

//    this.scanBtn.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        if(v.getId()==R.id.scan_button) {
//          IntentIntegrator scanIntegrator = new IntentIntegrator(this);
//          scanIntegrator.initiateScan();
//        }
//      }
//    });

    this.scanBtn.setOnClickListener(this);
  }

  public void onClick(View v){
    //respond to clicks
    if(v.getId()==R.id.scan_button){
      //scan
      IntentIntegrator scanIntegrator = new IntentIntegrator(this);
      scanIntegrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
      scanIntegrator.setPrompt("Scan a barcode");
      scanIntegrator.setCameraId(0);  // Use a specific camera of the device
      scanIntegrator.setBeepEnabled(false);
      scanIntegrator.setBarcodeImageEnabled(true);
      scanIntegrator.setOrientationLocked(true);
      scanIntegrator.initiateScan();
    } else if (v.getId()==R.id.btnSubmit) {
      formatTxt.setText("Number of Passenger : " + "1 people");
      contentTxt.setText("Passenger Number : " + etPassNumb.getText());
    }
  }

  public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    //retrieve scan result
    IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
    if (scanningResult != null) {
      //we have a result
      String scanContent = scanningResult.getContents();
      String scanFormat = scanningResult.getFormatName();
//      formatTxt.setText("Passenger Name : " + scanFormat);
      formatTxt.setText("Number of Passenger : " + "1 people");
      contentTxt.setText("Passenger Number : " + scanContent);
    } else {
      Toast toast = Toast.makeText(getApplicationContext(),
          "No scan data received!", Toast.LENGTH_SHORT);
      toast.show();
    }
  }

}
